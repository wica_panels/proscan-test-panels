## Purpose

This project was setup to facilitate ZPT tests using the GFA Wica Service.


## Repository Organisation

The following branches are defined:

* [dist_dev](https://gitlab.psi.ch/wica_panels/proscan-test-panels/-/tree/dist_dev) - Use for deployments to [Wica Development Server](https://gfa-wica-dev.psi.ch)
* [dist_prod](https://gitlab.psi.ch/wica_panels/proscan-test-panels/-/tree/dist_prod) - Use for deployments to [Wica Production Server](https://gfa-wica.psi.ch)
* [dist_ext](https://gitlab.psi.ch/wica_panels/proscan-test-panels/-/tree/dist_ext) - Use for deployments to [Wica External Server](https://wica.psi.ch)


## Deployment

After updating the relevant branch(es) autodeployment can be triggered by sending a request to the relevant autodeployment server(s) as follows:-

### Deploy to Wica Development Server
```
curl -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://gfa-wica-dev.psi.ch:8443"/"deploy?gitUrl=git@gitlab.psi.ch:wica_panels/proscan-test-panels.git"
```

### Deploy to Wica Production Server
```
curl -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://gfa-wica.psi.ch:8443"/"deploy?gitUrl=git@gitlab.psi.ch:wica_panels/proscan-test-panels.git"
```

### Deploy to Wica External Server
```
curl -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://wica.psi.ch:8443"/"deploy?gitUrl=git@gitlab.psi.ch:wica_panels/hipa-test-panels.git"
```

## Further documentation

- https://github.com/paulscherrerinstitute/wica-http
- https://github.com/paulscherrerinstitute/wica-js

## Support

Please contact simon.rees@psi.ch
